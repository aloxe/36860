import React from 'react';
import { Link } from "react-router-dom";
import Menu from './Menu';
import Meta from './Meta';
import { getCookie } from '../utils/cookies'
import './Home.css'

export default class User extends React.Component {

    componentDidMount() {
        if (this.props.match.params && this.props.match.params.id) {
            const userId = this.props.match.params.id;
            this.props.getPublicData("/api/uploads/visited-"+userId+"-communes.json", "userData");
            this.props.getPublicData("/api/uploads/visited-"+userId+"-unknown.json", "userFlawData");
        }
    }

    render() {
        const tableRequested = window.location.pathname.substr(this.props.match.params.id.length + 2)
        const stillUserDataLoading = allTrue(this.props.state.userLoading);
        // TODO: Meta données pour chaque tableaux (Arrays)
        return (
            <>
                <Meta
                    schema="ProfilePage"
                    title={"La France de "+this.props.state.userData.username}
                    description={"Données Eurobilltracker de "+ this.props.state.userData.username+" en France avec toutes ses communes visitées, prefectures, départements et carte"}
                    path={window.location.pathname.substring(1)}
                    contentType="website"
                />
                <Link to="/list" className="upload">Classement Général</Link>
                <Link to="/form" className="upload">Envoyez vos données</Link>

                {!stillUserDataLoading && <Menu state={this.props.state} tableRequested={tableRequested}  stateHandler={this.props.stateHandler}  handleMessage={this.handleHomeState} reqUser={this.props.state.userData} />}
            </>
        );
    }
}

function allTrue(obj) {
    for(var o in obj)
        if(obj[o]) return true;
    return false;
}
