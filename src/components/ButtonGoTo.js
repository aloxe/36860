import React from 'react';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/Button'

export function ButtonGoTo (props) {

    function handleClick(event) {
        window.location = event.srcElement.attributes.href;
    }

    return (
        <Button
            variant={props.variant ? props.variant : "primary"}
            id={props.id ? props.id : ""}
            href={props.location}
            onClick={handleClick}>
            Aller dans le forum
        </Button>
    );
}

ButtonGoTo.propTypes = {
    location: PropTypes.string.isRequired,
    variant: PropTypes.string,
    id: PropTypes.string
}
