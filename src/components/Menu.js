import React from 'react'
import PropTypes from 'prop-types'
import { Link } from "react-router-dom";
import Dropdown from 'react-bootstrap/Dropdown'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
// import CookieBox from './CookieBox'
import TableDisplay from './TableDisplay'
import { getCookie, setCookie } from '../utils/cookies'
import './Menu.css';


export default class Menu extends React.Component {

    constructor() {
        super();
        this.state = {
            tableRequested: "liste",
            fullMap: "",
            withCookie: getCookie("user") || false
        };
        this.handleClick = this.handleClick.bind(this);
        this.handleCheck = this.handleCheck.bind(this);
        this.handleChangeConsent = this.handleChangeConsent.bind(this);
    }

    handleClick(event) {
        this.setState({tableRequested: event.currentTarget.id });
    }

    handleCheck(event) {
        this.setState({ fullMap: event.target.checked ? "&v=full" : "" });
    }

    handleChangeConsent(e) {
        e.target.checked ? setCookie("user", this.props.state.user.id, 30) : setCookie("user", "", 1)
        this.setState({withCookie: e.target.checked});
    }

    componentDidMount() {
        document.getElementsByClassName("rowlinks")[0].style.display = 'none'
        document.getElementsByClassName("rowmenu")[0].style.display = 'flex'
    }

    render() {

        const url = "//ebt.fr.eu.org/carte.html#?u="+ this.props.reqUser.user +"&c="
        const tableRequested = this.props.tableRequested ? this.props.tableRequested : this.state.tableRequested

        const DropdownItemforForm = props => <Dropdown.Item id={props.id} href="#" onClick={this.handleClick}>{props.label}</Dropdown.Item>

        const DropdownItemforUser = props => <Link className="dropdown-item" id={props.id} to={props.id === 'liste' ? "/"+this.props.reqUser.user : "/"+this.props.reqUser.user+"/"+props.id} >{props.label}</Link>

        const DropdownItem = window.location.pathname === "/form" ? DropdownItemforForm : DropdownItemforUser;

        const MapDropdownItem = props => <Dropdown.Item className={props.children} href={url + props.children + this.state.fullMap}>{props.children}</Dropdown.Item>

        DropdownItem.propTypes = {
            id: PropTypes.string.isRequired,
            label: PropTypes.string.isRequired,
        }

        const CookieBox = (props) => {
            return (
            <div className="text-center block">
                <Form.Group controlId="saveCookie" onChange={this.handleChangeConsent}>
                    <Form.Check type="checkbox" label="Sauvegarder mon ID Eurobilltracker pendant 1 mois" defaultChecked={props.checked}/>
                    (Permet d'éditer les erreurs sans avoir à renvoyer un fichier)
                </Form.Group>
            </div>
        )}

        const problemes = ["erreurs", "doublons"].includes(tableRequested);
        const communes = ["tableau", "liste"].includes(tableRequested);
        const jeux = ["communes", "tourdefrance", "prefectures"].includes(tableRequested);

        return (

            <div className="block">
            <div className="rowlinks">
                <Button className="public" variant="outline-secondary">{this.props.reqUser.userflag} {this.props.reqUser.username}</Button>
                <DropdownItem id="liste" label="🗒 Liste brute" /> <DropdownItem id="tableau" label="🗓 Joli tableau" /> <DropdownItem id="communes" label="🇫🇷 36000 communes" /> <DropdownItem id="tourdefrance" label="🚴 Tour de France" /> <DropdownItem id="prefectures" label="🏛 Jeu des préfectures" />
            </div>
            <div className="text-center block not-public">
            <CookieBox checked={this.state.withCookie} />
            </div>
            <div className="row rowmenu" id="menu">
                Afficher: 
                <Button className="public" variant="outline-secondary">{this.props.reqUser.userflag} {this.props.reqUser.username}</Button>
                <Dropdown className="btn-group not-public">
                    <Dropdown.Toggle variant={problemes ? "light" : "outline-primary"}>⚠ Les problèmes</Dropdown.Toggle>
                    <Dropdown.Menu>
                        <DropdownItem id="erreurs" label="⚠ Les erreurs" />
                        <DropdownItem id="doublons" label="⚠ Les doublons" />
                    </Dropdown.Menu>
                </Dropdown>
                <Dropdown className="btn-group">
                    <Dropdown.Toggle variant={communes ? "light" : "outline-primary"}>Liste des communes</Dropdown.Toggle>
                    <Dropdown.Menu>
                        <DropdownItem id="liste" label="🗒 Liste brute" />
                        <DropdownItem id="tableau" label="🗓 Joli tableau" />
                    </Dropdown.Menu>
                </Dropdown>
                <Dropdown className="btn-group">
                    <Dropdown.Toggle variant={jeux ? "light" : "outline-primary"}>Les jeux</Dropdown.Toggle>
                    <Dropdown.Menu>
                        <DropdownItem id="communes" label="🇫🇷 36000 communes" />
                        <DropdownItem id="tourdefrance" label="🚴 Tour de France" />
                        <DropdownItem id="prefectures" label="🏛 Jeu des préfectures" />
                    </Dropdown.Menu>
                </Dropdown>
                <Dropdown className="btn-group">
                    <Dropdown.Toggle variant="outline-primary">La carte</Dropdown.Toggle>
                    <Dropdown.Menu className="dropdown-map" id="carte" user={this.props.reqUser.user}>
                        <label><input type="checkbox" onChange={this.handleCheck}></input>Toutes communes</label>
                        <MapDropdownItem>blue</MapDropdownItem>
                        <MapDropdownItem>indigo</MapDropdownItem>
                        <MapDropdownItem>purple</MapDropdownItem>
                        <MapDropdownItem>darkgreen</MapDropdownItem>
                        <MapDropdownItem>red</MapDropdownItem>
                        <MapDropdownItem>black</MapDropdownItem>
                        <MapDropdownItem>teal</MapDropdownItem>
                        <MapDropdownItem>orangered</MapDropdownItem>
                        <MapDropdownItem>snow</MapDropdownItem>
                        <MapDropdownItem>navy</MapDropdownItem>
                        <MapDropdownItem>maroon</MapDropdownItem>
                        <MapDropdownItem>olive</MapDropdownItem>
                    </Dropdown.Menu>
                </Dropdown>
            </div>
            <TableDisplay tableRequested={tableRequested ? tableRequested : "liste"} state={this.props.state}
                stateHandler={this.props.stateHandler} handleMessage={this.props.handleMessage} />
            </div>
        );
    }
}
