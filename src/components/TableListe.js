import React from 'react';
import Table from 'react-bootstrap/Table';
import { String } from '../utils/strings'

export function TableListe (props) {

        const userData = props.userData
        const visited = userData.communes

        visited.sort((a,b) => (a.nomCommune.sansAccent().toLowerCase() > b.nomCommune.sansAccent().toLowerCase()) ? 1 : ((b.nomCommune.sansAccent().toLowerCase() > a.nomCommune.sansAccent().toLowerCase()) ? -1 : 0));

        return (
            <Table bordered>
                <thead>
                    <tr><th>
                    <h3>Les {visited.length} communes visitées par {userData.userflag} {userData.username}</h3>
                    </th></tr>
                </thead>
                <tbody>
                    <tr><td>
                        {visited.map(commune  => (
                            <span key={commune.codeCommune.toString()}>{commune.nomCommune} </span>
                        ))}
                    </td></tr>
                </tbody>
            </Table>
        );
}
