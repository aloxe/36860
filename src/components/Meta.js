// source https://github.com/stereobooster/an-almost-static-stack/blob/react-snap/src/components/Seo.js
import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

const absoluteUrl = path => `http://ebt.fr.eu.org/${path}`;
const seoImageURL = file => `http://ebt.fr.eu.org/images/${file}`;

const getMetaTags = ({
  title, description, url, contentType, published, updated, category, tags, twitter, image,
}) => {
  const metaTags = [
    { itemprop: 'name', content: title },
    { itemprop: 'description', content: description },
    { name: 'description', content: description },
    { name: 'twitter:title', content: `${title} | EBT Carte de France` },
    { name: 'twitter:description', content: description },
    { name: 'twitter:creator', content: twitter || '@aloxecorton' },
    { name: 'og:title', content: `${title} | EBT Carte de France` },
    { name: 'og:type', content: contentType },
    { name: 'og:url', content: url },
    { name: 'og:description', content: description },
    { name: 'og:site_name', content: 'EBT Carte de France' },
    { name: 'og:locale', content: 'fr_FR' },
    // { name: 'fb:app_id', content: '<FB App ID>' },
  ];

  if (image) {
    metaTags.push({ itemprop: 'image', content: seoImageURL(image) });
    metaTags.push({ name: 'twitter:image:src', content: seoImageURL(image) });
    metaTags.push({ name: 'og:image', content: seoImageURL(image) });
    metaTags.push({ name: 'twitter:card', content: 'summary_large_image' });
  } else {
    metaTags.push({ name: 'twitter:card', content: 'summary' });
  }

  return metaTags;
};

const getHtmlAttributes = ({
  schema
}) => {
  let result = {
    lang: 'fr',
  };
  if (schema) {
    result = {
      ...result,
      itemscope: undefined,
      itemtype: `http://schema.org/${schema}`,
    }
  }
  return result;
}

getHtmlAttributes.propTypes = {
  schema: PropTypes.string,
};

const Meta = ({
  schema, title, description, path, contentType, published, updated, category, tags, twitter, image
}) => (
  <Helmet
    htmlAttributes={getHtmlAttributes({
      schema,
    })}
    title={ title }
    link={[
      { rel: 'canonical', href: absoluteUrl(path) },
    ]}
    meta={getMetaTags({
      title,
      description,
      contentType,
      url: absoluteUrl(path),
      published,
      updated,
      category,
      tags,
      twitter,
      image
    })}
  />
);

Meta.propTypes = {
  schema: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  path: PropTypes.string,
  contentType: PropTypes.string,
  twitter: PropTypes.string,
  image: PropTypes.string,
};

export default Meta;
