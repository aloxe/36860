import React, { useEffect, useState } from 'react'
import { CSVReader } from 'react-papaparse'
import { Spinner } from 'react-bootstrap'
import Form from 'react-bootstrap/Form'
import { getExtractCommunes } from '../utils/communes'
import { getResults36000 } from '../utils/36000'
import { uploadJson, getUserFromId, addLeaderToBoard } from '../utils/files'
import { setCookie, getCookie } from '../utils/cookies'
import './CSVDropReader.css';

export function CSVDropReader (props) {

    const [variant, setVariant] = useState('light');
    const [withCookie, setwithCookie] = useState(false);

    function handleChangeConsent(e) {
        setwithCookie(e.target.checked)
    }

    function handleOnDrop(data, meta) {
        setVariant('primary');
        props.handleMessage("message", "Réception du fichier 💶 💶 💶 💶 💶")
        var userId = meta.name.match(/_entered_by_([0-9][0-9]*)/);
        userId = (userId !== null) ? userId[1] : "ANONYME";
        withCookie && setCookie("user", userId, 30)
        getUserFromId(userId, props.stateHandler, props.handleMessage);

        processExtractCommunes(data, props.state).then((results) => {
            props.handleMessage("message", "💶→🏭 Extraction des communes ")
            props.stateHandler("userData", results)
        }).catch((error) => {
            console.log('Error from processDataAsycn() with async( When promise gets rejected ): ' + error)
            props.handleMessage("message", "Erreur pendant l'extraction des communes" + error)
        });

    }

    function handleOnError (err, file, inputElem, reason) {
        setVariant('light');
        console.log(err)
        props.handleMessage("message", "Erreur lors de l'envoi du fichier" + err)
    }

    useEffect(() => {
        let isMounted = true;
        if (props.state.user.id && props.state.dataUpoading.dataUser) {
            var newdataUploading = props.state.dataUpoading
            newdataUploading.dataUser = false;
            isMounted && props.stateHandler("dataUpoading", newdataUploading)
        }

        if (props.state.user.id && props.state.userData.length > 1 && props.state.dataUpoading.dataMemory) {
            var novdataUploading = props.state.dataUpoading
            novdataUploading.dataMemory = false;
            isMounted && props.stateHandler("dataUpoading", novdataUploading)

            const visitedCommunes = {
                user: props.state.user.id,
                username: props.state.user.name,
                userflag: props.state.user.flag,
                nombre: (props.state.userData[0].length),
                population: getResults36000(props.state.userData[0])[1].total,
                superficie: getResults36000(props.state.userData[0])[2].total,
                communes: props.state.userData[0],
                date: parseInt(Date.now()/1000),
                type: "communes"
            };
            uploadJson(visitedCommunes, props.handleMessage)
            isMounted && props.stateHandler("userData", visitedCommunes)

            const leader = {
                user: props.state.user.id,
                userflag: props.state.user.flag,
                username: props.state.user.name,
                nombre: (props.state.userData[0].length),
                population: getResults36000(props.state.userData[0])[1].total,
                superficie: getResults36000(props.state.userData[0])[2].total,
                date: parseInt(Date.now()/1000),
                type: "visites"
            };
            const newLeaderBoard = addLeaderToBoard(leader, props.state.leaderboard)
            uploadJson(newLeaderBoard, props.handleMessage)
            isMounted && props.stateHandler("leaderboard", newLeaderBoard)

            const visitedUnknown = {
                user: props.state.user.id,
                username: props.state.user.name,
                userflag: props.state.user.flag,
                nombre: (props.state.userData[1].length),
                lieux: props.state.userData[1],
                date: parseInt(Date.now()/1000),
                type: "unknown"
            };
            uploadJson(visitedUnknown, props.handleMessage)
            isMounted && props.stateHandler("userFlawData", visitedUnknown)

            var novodataUploading = props.state.dataUpoading
            novodataUploading.dataCommunes = false;
            novodataUploading.dataUnknown = false;
            isMounted && props.stateHandler("dataUploading", novodataUploading)
        }
        return () => { isMounted = false };
    }, [props]);

    const MenuSpinner = props => <div><Spinner animation="border" role="status" size="lg"
        variant={props.variant}><span className="sr-only">Upoading...</span></Spinner></div>;

    const hasCookie = getCookie("user");

    return (
        <div className="text-center block">
            <Form.Group controlId="saveCookie" onChange={handleChangeConsent}>
                <Form.Check type="checkbox" label="Sauvegarder mon ID Eurobilltracker pendant 1 mois" defaultChecked={hasCookie} />
                (Permet d'éditer les erreurs sans avoir à renvoyer un fichier)
            </Form.Group>
            <CSVReader
                onClick={() => {setVariant('info')}}
                onDrop={handleOnDrop}
                onError={handleOnError}
                style={{}}
                config={{}}
            >
            <span>Glissez votre fichier CSV ici ou cliquez pour le téléverser.</span>
            </CSVReader>
            <MenuSpinner variant={variant} />
        </div>
        )
}

const processExtractCommunes = async (data, state) => {
    return getExtractCommunes(data, state);
};
