import React, { Fragment } from 'react';
import Table from 'react-bootstrap/Table';
import Dropdown from 'react-bootstrap/Dropdown'
import PropTypes from 'prop-types';
import { LinkBankNote } from './LinkBankNote';
import {
    checkCommuneSansCodePostal,
    allCommunesByCodePostal,
    getCommuneByInsee,
    groupParisLyonMarseille,
    getPopulation,
    getSuperficie,
    getAltitude
 } from '../utils/communes'
import { String } from '../utils/strings'
import { uploadJson, addLeaderToBoard } from '../utils/files'
import './TableErrors.css';

export function TableErrors (props) {

    const codesPostaux = props.state.codesPostaux;
    const userFlawData = props.state.userFlawData
    const visited = userFlawData.lieux
    const visitedProb = visited.filter(el => (el.type !== "double") )
    const visitedDoub = visited.filter(el => (el.type === "double") )
    const doublon = props.type === "doublons" ? true : false;

    function handleClick(event) {
        const val = event.target.dataset;
        const selectedCommune = getCommuneByInsee(val.id, codesPostaux);
        let newEbtLieu = {};
        if (selectedCommune)
            newEbtLieu = {
                "codeCommune": val.id,
                "codePostal": val.cp,
                "nomCommune": selectedCommune.nomCommune,
                "nomEBT": val.nom,
                "ref": val.note
            };
        let newEbtNames = props.state.ebtNames;
        let newLieux = props.state.ebtNames.lieux;
        newLieux.push(newEbtLieu);
        newEbtNames.nombre = (newLieux.length)
        newEbtNames.lieux = newLieux

        // location name saved in EBT names
        props.stateHandler("ebtNames", newEbtNames)
        uploadJson(newEbtNames, props.handleMessage)
        props.handleMessage("message", "👍👍 Il y a maintenant " + newLieux.length + " noms dans la liste des lieux EBT")

        let newCommunes = props.state.userData.communes;
        // one match means the commune is already counted
        newLieux = newCommunes.filter( el => (el.codeCommune === val.id) );
        if (newLieux.length < 1) {
            let newVisitedCommunes = props.state.userData;
            let newCommune = getCommuneByInsee(newEbtLieu.codeCommune, codesPostaux);
            newCommune.ref = val.note
            newCommune.population = getPopulation(newCommune.codeCommune, props.state.population)
            newCommune.superficie = getSuperficie(newCommune.codeCommune, props.state.superficie)
            newCommune.altitude = getAltitude(newCommune.codeCommune, props.state.altitude)
            newCommunes.push(newCommune);

            newCommunes = groupParisLyonMarseille(newCommunes)
            newVisitedCommunes.communes = newCommunes
            newVisitedCommunes.nombre = newCommunes.length
            newVisitedCommunes.population += parseInt(newCommune.population)
            newVisitedCommunes.superficie = parseFloat(newVisitedCommunes.superficie) + parseFloat(newCommune.superficie)
            newVisitedCommunes.date = parseInt(Date.now()/1000)

            // location name saved in userData users' list
            props.stateHandler("userData", newVisitedCommunes)
            uploadJson(newVisitedCommunes, props.handleMessage)
            props.handleMessage("message", "Avec " + newEbtLieu.nomCommune + ", ça fait maintenant " + newCommunes.length + " communes 🙌")

            const leader = {
                user: newVisitedCommunes.user,
                userflag: newVisitedCommunes.userflag,
                username: newVisitedCommunes.username,
                nombre: newVisitedCommunes.nombre,
                population: newVisitedCommunes.population,
                superficie: newVisitedCommunes.superficie,
                type: "visites",
                date: parseInt(Date.now()/1000)
            };
            const newLeaderBoard = addLeaderToBoard(leader, props.state.leaderboard)
            uploadJson(newLeaderBoard, props.handleMessage)
            props.stateHandler("leaderboard", newLeaderBoard)
            props.handleMessage("message", "Classement général mis à jour 👏👏👏")

            let novVisitedUnknown = userFlawData;
            let novLieux = userFlawData.lieux;
            novLieux = novLieux.filter( el => (el.ref !== newEbtLieu.ref) );
            novVisitedUnknown.nombre = novLieux.length
            novVisitedUnknown.lieux = novLieux
            // location name removed or updated from problems
            props.stateHandler("userFlawData", novVisitedUnknown)
            uploadJson(novVisitedUnknown, props.handleMessage)
            props.handleMessage("message", "plus que " + novLieux.length + " problèmes")
        } else {
            props.handleMessage("message", "La commune " + newEbtLieu.nomCommune + " est déjà comptée 🤷 ça va faire un doublon")

            let novVisitedUnknown = userFlawData;
            let novLieux = userFlawData.lieux;
            for (var i=0; i<novLieux.length; i++) {
                if (novLieux[i].ref === val.note) {
                    novLieux[i].type = "double";
                    novLieux[i].possibleCommune = newLieux[0]
                }
            }
            // location name removed or updated from problems
            props.stateHandler("userFlawData", novVisitedUnknown)
            uploadJson(novVisitedUnknown, props.handleMessage)
        }
    }

    function defineActionVar(visit) {
        switch(visit.type) {
            case 'double':
                return [visit.possibleCommune.nomCommune];
            case 'wrong':
                return checkCommuneSansCodePostal(visit.location.nomCommune, visit.location.codePostal, codesPostaux) ? [visit.location.codePostal, checkCommuneSansCodePostal(visit.location.nomCommune, visit.location.codePostal, codesPostaux)] : [visit.location.codePostal];
            case 'unknown':
                return [visit.location.codePostal];
            case 'multiple':
                return [visit.location.codePostal];
            default:
                return [];
        }
    }

    function formSelectCommune(visit) {
        var possibleCommunes = visit.type === "multiple" ? visit.possibleCommunes
            : allCommunesByCodePostal(visit.location.codePostal, codesPostaux);
        if (possibleCommunes.length < 1)
            return undefined
        else {
            return (
                <Dropdown className="btn-group">
                    <Dropdown.Toggle>… choisir …</Dropdown.Toggle>
                    <Dropdown.Menu>
                    {possibleCommunes.map(possibleCommune => (
                        <Dropdown.Item
                        key = {possibleCommune.codeCommune}
                        id = {possibleCommune.codeCommune}
                        data-note={visit.ref}
                        data-nom={visit.location.nomCommune}
                        data-cp={visit.location.codePostal}
                        data-id={possibleCommune.codeCommune}
                        href="#"
                        onClick={handleClick}>
                            {possibleCommune.nomCommune}
                        </Dropdown.Item>
                    ))}
                    </Dropdown.Menu>
                </Dropdown>
            )
        }
    }

    return (
        <Table bordered>
            <thead>
                <tr><th colSpan='4'>
                    {doublon
                        ? <h3>Les {visitedDoub.length} lieux faisant doublon</h3>
                        : <h3>Les {visitedProb.length} lieux posant problème </h3>
                    }
                </th></tr>
            </thead>
            <tbody>
            {doublon ? <tr className="furow"><td  colSpan='4'>Vous pouvez corriger les nom dans EBT mais vous pouvez aussi décider de garder le nom des hameaux, zac, aéroports, communes associées… tels qu'ils sont.</td></tr> : []}
                <Fragment>
                    {visited.map(visit => (
                        <Fragment key={visit.location.nomCommune+visit.location.codePostal.toString()}>
                        <tr className={(doublon === (visit.type !== "double")) ? "hide" : "show"} key={visit.location.codePostal.toString()}>
                            <td><h4>{visit.location.codePostal}  {visit.location.nomCommune}</h4></td>
                            <td><b>{texts.display(visit.type+"_title", [])}</b> <span title={texts.display(visit.type+"_text", [])} className='round' role="img" aria-label="?">❓</span></td>
                        <td className="bouton"><a href={"https://www.openstreetmap.org/search?whereami=1&query="+ visit.map.lat +"%2C"+ visit.map.long +"#map=12/"+ visit.map.lat +"/"+ visit.map.long} title='voir la commune sur la carte' className='lead' target='new'>🗺<br/><span>OpenStreetMap</span></a></td>
                            <td><LinkBankNote noteRef={visit.ref} /></td>
                        </tr>
                        <tr className={(doublon === (visit.type !== "double")) ? "hide furow" :"show furow"} key={"FR"+visit.location.codePostal.toString()}>
                            <td  colSpan='4'>
                                <i>{texts.display(visit.type+"_action", defineActionVar(visit))}</i><br/>
                                {["wrong", "multiple", "unknown"].includes(visit.type) && formSelectCommune(visit)}
                            </td>
                        </tr>
                        </Fragment>
                    ))}
                </Fragment>
            </tbody>
        </Table>
    );

    Dropdown.Item.propTypes = {
        note: PropTypes.string.isRequired,
        nom: PropTypes.string.isRequired,
        cp: PropTypes.string.isRequired,
        id: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired
    }
}

const texts = {
    display: function(notification, arrayVar) {
        var messageText = this[notification](arrayVar);
        return messageText;
    },

    bad_title: function(ar) { return "Mauvais code postal"; },

    bad_text: function(ar) { return "Ce code postal n'est pas au format ISO (il doit être formé de 5 chiffres)."; },

    bad_action: function(ar) { return "Corrigez le code postal dans EBT avec le ilen sur le billet."; },

    multiple_title: function(ar) { return "Choix multiple"; },

    multiple_text: function(ar) { return "Ce lieu peut correspondre à plusieurs communes avec le même code postal."; },

    multiple_action: function(ar) { return "Vérifiez le nom du lieu et corrigez-les dans EBT. Sinon vous pouvez rattacher ce nom à l'une des communes correspondante."; },

    wrong_title: function(ar) { return "Mauvais code postal"; },

    wrong_text: function(ar) { return "Ce lieu existe avec le même nom mais un autre code postal. Il s'aggit surrement d'une erreur à corriger dans vos enregistrements sur EBT."; },

    wrong_action: function(ar) { return "Le bon code postal de "+ar[2]+" est "+ar[1]+". Vérifiez le code postal, vérifiez le nom du lieu et corrigez-les dans EBT. Sinon vous pouvez rattacher ce hameau à une commune ayant le code postal " +ar[0]+ "."; },

    unknown_title: function(ar) { return "Inconnu"; },

    unknown_text: function(ar) { return "Ce lieu ne correspond à aucune commune répertoriée avec ce code postal . Peut-être est-ce un hameau ou une faute de code postal ou de nom."; },

    unknown_action: function(ar) { return "Vérifiez le code postal, vérifiez le nom du lieu et corrigez-les dans EBT. Sinon vous pouvez rattacher ce hameau à une commune ayant le code postal " +ar[0]+ "."; },

    double_title: function(ar) { return "Doublon"; },

    double_text: function(ar) { return "Sauf code postal éronné, ce lieu correspond déjà à une commune visitée. Il s´aggit peut-être d´un hameau, d´une commune associée, ou d'un autre lieu dont le nom est accepté par EBT."; },

    double_action: function(ar) { return "Sauf code postal éronné, ce lieu correspond déjà à une commune visitée. Le nom officiel de la commune est " + ar[0] +"."; }
}
