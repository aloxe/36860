import React from 'react';
import { Splash } from './Splash';
import Form from './Form';
import Home from './Home';
import User from './User';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { getCookie } from '../utils/cookies'
import './App.css';

export default class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            population: [],
            superficie: [],
            altitude: [],
            departementsFrancais: [],
            codesPostaux: [],
            ebtNames: [],
            userData: [],
            userFlawData: [],
            leaderboard: [],
            loading: {
                population: true,
                superficie: true,
                altitude: true,
                departementsFrancais: true,
                codesPostaux: true,
                ebtNames: true
            },
            userLoading: {
                userData: true,
                userFlawData: true
            },
            dataUpoading: {
                dataUser: true,
                dataMemory: true,
                dataCommunes: true,
                dataUnknown: true
            },
            user: {
                id: "",
                name: "",
                flag: "",
                logged: false,
                super: false
            },
        }
        this.getPublicData = this.getPublicData.bind(this)
        this.handleState = this.handleState.bind(this)
    }

    handleState(key, value) {
        this.setState({[key]: value});
    }

    getPublicData (url, key) {
        fetch(url)
            .then(response => {
                if (response.status >= 200 && response.status <= 299) {
                    return response.json();
                } else {
                    // console.log(response.status, response.statusText);
                    throw Error(response.statusText);
                }
            })
            .then(data => {
                this.setState({[key]: data});

                if ([key].toString().slice(0,4) !== 'user') {
                    const loaded = this.state.loading;
                    loaded[key] = false;
                    this.setState({loading: loaded});
                } else {
                    const loaded = this.state.userLoading;
                    loaded[key] = false;
                    this.setState({userLoading: loaded});
                }
            })
            .catch(error => console.error(url, error));
    }

    componentDidMount() {
        this.getPublicData("/api/uploads/leaderboard_users.json", "leaderboard");
        this.getPublicData("/api/geodata/communes-population-2019.json", "population");
        this.getPublicData("/api/geodata/villes_france-2019.json", "superficie");
        this.getPublicData("/api/geodata/correspondance-code-insee-altitude-2013.json", "altitude");
        this.getPublicData("/api/geodata/departments_regions_france_2017.json", "departementsFrancais");
        this.getPublicData("https://unpkg.com/codes-postaux@3.3.0/codes-postaux.json", "codesPostaux");
        this.getPublicData("/api/uploads/geodata/ebt_names_communes_france.json", "ebtNames");

        const cookieUser = getCookie("user");
        if (cookieUser) {
            // leaderboard not yet loaded
            const superUser = (cookieUser === "31378" || cookieUser === "39850") ? true : false;
            const savedUser = {
                id: cookieUser,
                name: "",
                flag: "",
                logged: true,
                super: superUser
            }
            this.handleState("user", savedUser)
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.user.logged && this.state.user.name === "") {
            if (prevState.leaderboard !== this.state.leaderboard) {
                const savedUser = this.state.user
                const loggedUser = this.state.leaderboard.filter(el => (el.user === savedUser.id) )[0]
                savedUser.name = loggedUser.username
                savedUser.flag = loggedUser.userflag
            }
        }
    }

    isPrivatePage() {
        return this.state.user.super || (this.state.user.logged && window.location.pathname.substr(1) === this.state.user.id)
    }

    render() {
        const Formpage = props => <Form state={this.state}
            stateHandler={this.handleState}
            getPublicData={this.getPublicData}
            key={window.location.pathname}
            {...props} />

        const Homepage = props => <Home state={this.state}
            stateHandler={this.handleState}
            getPublicData={this.getPublicData}
            key={window.location.pathname}
            {...props} />

        const Userpage = props => <User state={this.state}
            stateHandler={this.handleState}
            getPublicData={this.getPublicData}
            key={window.location.pathname}
            {...props} />

        function isFormPage() {
            return window.location.pathname === "/form";
        }

        return (
            <Router>
                <header>
                    <h1 className="title">
                        <img src="/images/france.png" width="48" height="48" alt="France" /> Ma carte de France Eurobilltracker
                    </h1>
                    {this.state.user.logged && <div className="user">{this.state.user.name}</div>}
                </header>
                <div className={ this.isPrivatePage() || isFormPage() ? "container" : "public container"}>
                    <Switch>
                        <Route exact path="/form" render={Formpage}/>
                        <Route exact path="/list" render={Homepage}/>
                        <Route exact path="/listforum" render={Homepage}/>
                        <Route exact path="/:id" render={Userpage}/>
                        <Route exact path="/:id/liste" render={Userpage}/>
                        <Route exact path="/:id/tableau" render={Userpage}/>
                        <Route exact path="/:id/communes" render={Userpage}/>
                        <Route exact path="/:id/tourdefrance" render={Userpage}/>
                        <Route exact path="/:id/prefectures" render={Userpage}/>
                        <Route exact path="/:id/erreurs" render={Userpage}/>
                        <Route exact path="/:id/doublons" render={Userpage}/>
                        <Route exact path="/"><Splash /></Route>
                    </Switch>
                </div>
            </Router>
        );
    }
}
