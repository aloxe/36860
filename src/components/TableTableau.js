import React, { Fragment } from 'react';
import Table from 'react-bootstrap/Table';
import { LinkBankNote } from './LinkBankNote';
import String from '../utils/strings'

export function TableTableau (props) {

    const userData = props.userData
    const visited = userData.communes
    const departementsFrancais = props.departementsFrancais

    const CommunesDuDept = props => (
        <Fragment>
        {props.visited.map(visit => (
            <tr key={visit.codeCommune.toString()}>
                <td>
                    {visit.codePostal}
                </td><td>
                    {visit.nomCommune}
                </td><td>
                    {visit.superficie.toLocaleString('fr-FR')} km²<br/>
                    {visit.population.toLocaleString('fr-FR')} hab.
                </td><td>
                    <LinkBankNote noteRef={visit.ref} />
                </td>
            </tr>
        ))}
        </Fragment>
    )

    visited.sort((a,b) => (a.nomCommune.sansAccent().toLowerCase() > b.nomCommune.sansAccent().toLowerCase()) ? 1 : ((b.nomCommune.sansAccent().toLowerCase() > a.nomCommune.sansAccent().toLowerCase()) ? -1 : 0));

    return (
        <Table bordered>
            <thead>
                <tr><th colSpan='4'>
                <h3>Les {visited.length} communes visitées par {userData.userflag} {userData.username}</h3>
                </th></tr>
            </thead>
            <tbody>
                {departementsFrancais.map(departement  => (
                    <Fragment key={departement.departmentCode.toString()}>
                        <tr>
                            <th colSpan='3'>{departement.departmentName} ({departement.departmentCode})</th>
                            <th colSpan='1'>{visited.filter(function(visit){
                            return visit.codeCommune.substr(0, 2).toUpperCase() === departement.departmentCode.toUpperCase() }).length}</th>
                        </tr>
                        <CommunesDuDept departement={departement.departmentCode} visited={visited.filter(function(visit){
                            return visit.codeCommune.substr(0, 2).toUpperCase() === departement.departmentCode.toUpperCase() })
                                ? visited.filter(function(visit){
                                return visit.codeCommune.substr(0, 2).toUpperCase() === departement.departmentCode.toUpperCase() })
                                : null} />
                    </Fragment>
                ))}
            </tbody>
        </Table>
    );
}
