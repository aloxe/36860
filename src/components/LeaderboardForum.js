import React from 'react';
import { Link } from "react-router-dom";
import Table from 'react-bootstrap/Table';
import { timeConverter } from '../utils/strings'
import { POPULATION, SUPERFICIE, COMMUNES, POPULATION_META, COMMUNES_META } from '../utils/constants'
import {ReactComponent as FranceIcon} from "../assets/france.svg"
import { ButtonCopy } from './ButtonCopy';
import { ButtonGoTo } from './ButtonGoTo';
import './Leaderboard.css';

export function LeaderboardForum(props) {

    const users = props.users.sort((a, b) => (a.nombre < b.nombre) ? 1 : ((b.nombre < a.nombre) ? -1 : 0));
    const userlist = users.filter(el => (el.username !== "anonymous") )

    const usersPop = props.users.sort((a, b) => (a.population < b.population) ? 1 : ((b.population < a.population) ? -1 : 0));
    const userlistPop = usersPop.filter(el => (el.username !== "anonymous") )

    const usersSup = props.users.sort((a, b) => (a.superficie < b.superficie) ? 1 : ((b.superficie < a.superficie) ? -1 : 0));
    const userlistSup = usersPop.filter(el => (el.username !== "anonymous") )

    var display_code = "[size=150]Contexte :[/size]\n";
    display_code += "Les pourcentages sont calculés par rapport aux "+ COMMUNES.toLocaleString('fr-FR') +" communes ([url="+COMMUNES_META.source+"]source[/url]), aux " + POPULATION.toLocaleString('fr-FR') + " habitants ([url="+POPULATION.source+"]source[/url]) et " + SUPERFICIE.toLocaleString('fr-FR') + "km² ([url="+SUPERFICIE.source+"]source[/url]) que compte la France en "+COMMUNES_META.year+".\n\n";

    display_code += "[size=150]Classement par communes :[/size]\n";

    userlist.forEach(async (user, i)  => {
        // console.log("plop " + i + user.userflag + " " + user.username )
        var date = " (" + user.date ? timeConverter(user.date) : "avant" + ")"
        if (user.nombre && user.nombre > 0)
            display_code += i+1+ ") " + user.userflag + " " + user.username + "............ " + user.nombre.toLocaleString('fr-FR') + " soit " + (user.nombre*100/COMMUNES).toFixed(2) + "% [size=85]" + date + "[/size]\n"
    })
    display_code += "\n[size=150]Classement par population :[/size]\n";

    userlistPop.forEach(async (user, i)  => {
        // console.log("plop " + i + user.userflag + " " + user.username )
        var date = " (" + user.date ? timeConverter(user.date) : "avant" + ")"
        if (user.population && user.population > 0)
            display_code += i+1+ ") " + user.userflag + " " + user.username + "............ " + user.population.toLocaleString('fr-FR') + " soit " + (user.population*100/POPULATION).toFixed(2) + "% [size=85]" + date + "[/size]\n"
    })

    display_code += "\n[size=150]Classement par superficie :[/size]\n";

    userlistSup.forEach(async (user, i)  => {
        // console.log("plop " + i + user.userflag + " " + user.username )
        var date = " (" + user.date ? timeConverter(user.date) : "avant" + ")"
        var superficie = parseFloat(user.superficie).toFixed(2)
        if (user.superficie && user.superficie > 0)
            display_code += i+1+ ") " + user.userflag + " " + user.username + "............ " + parseFloat(superficie).toLocaleString('fr-FR') + " km² soit " + (user.superficie*100/SUPERFICIE).toFixed(2) + "% [size=85]" + date + "[/size]\n"
    })
    return (
        <Table bordered>
            <thead>
                <tr><th colSpan='4'>
                <h3>Classement pour le forum</h3>
                </th></tr>
            </thead>
            <tbody>
            <tr className="not-public"><td><textarea name='message' id='code-forum' rows='8' cols='76' tabIndex='4' className='md-textarea form-control' defaultValue={display_code} /></td></tr>
            <tr className="not-public"><td>
                <ButtonCopy />
                <ButtonGoTo location='https://forum.eurobilltracker.com/posting.php?mode=reply&f=34&t=7171' />
            </td></tr>
            </tbody>
        </Table>
    )
}
