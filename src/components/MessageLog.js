import React from 'react';
import './MessageLog.css';

export function MessageLog(props)  {

    const loading = props.loading;

    const message_population = loading.population ? "📥 chargement des données population"
        : "🗃️ population OK ☑";
    const message_superficie = loading.superficie ? "📥 chargement des données superficie"
        : "🗃️ superficie OK ☑" ;
    const message_altitude = loading.altitude ? "📥 chargement des données altitude"
        : "🗃️ altitude OK ☑";
    const message_departementsFrancais = loading.departementsFrancais ? "📥 chargement des données départements"
        : "🗃️ départements OK ☑";
    const message_codesPostaux = loading.codesPostaux ? "📥 chargement des données codes postaux"
        : "🗃️ codes postaux OK ☑";
    const message_ebtNames = loading.ebtNames ? "📥 chargement des données des lieux EBT"
        : "🗃️ lieux EBT OK ☑";

    const message_array = props.messages;

    return (
        <div className="messages">
            <div id="messages_inside">
            {message_population}<br/>
            {message_superficie}<br/>
            {message_altitude}<br/>
            {message_departementsFrancais}<br/>
            {message_codesPostaux}<br/>
            {message_ebtNames}<br/>
            {message_array.map( (message, index)  => (
                <div key={index}>{message} </div>
            ))}
        </div>
        </div>
    );
}
