import React, { useState, useRef } from 'react'
import styled from 'styled-components'
import { VectorMap } from '@south-paw/react-vector-maps'
import france from '../assets/france-dept.json'
import './CarteSvg.css';

export function CarteSvg (props) {

    const Map = styled.div`
      margin: 1rem auto;
      width: ${props => props.width || "100px"};

      svg {
        stroke: white;

        // All layers are just path elements
        path {
          fill: navy;
          cursor: pointer;
          outline: none;

          // When a layer is 'checked' (via checkedLayers prop).
          &[aria-checked='true'] {
            fill: crimson;
          }

          // When a layer is hovered
          &:hover {
            fill: aliceblue;
            stroke: navy;
          }

        }
      }
    `;

    const [hovered, setHovered] = useState('');
    const tooltip = useRef();

    const layerProps = {
        onMouseOver: (e) => handleMouseOver(e),
        onMouseOut: () => handleMouseOut()
     };

    const Tooltip = () => (<span ref={tooltip} className="tooltip" style={{ top: '0', left: '0' }}>{hovered}</span>)

    function handleMouseOver(e) {
        setHovered(e.target.attributes.name.value);
        const tooltipBox = tooltip.current;
        tooltipBox.style.left = e.pageX + 'px';
        tooltipBox.style.top = e.pageY + 'px';
        tooltipBox.style.opacity = 1;
    }

    function handleMouseOut() {
        const tooltipBox = tooltip.current;
        tooltipBox.style.opacity = 0;
    }

    return (
        <Map width={props.width} id="mapfr">
            <VectorMap {...france} checkedLayers={props.visitedDepartements} layerProps={layerProps} />
            <Tooltip />
        </Map>
    );
}
