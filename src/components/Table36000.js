import React from 'react';
import Table from 'react-bootstrap/Table';
import { ButtonCopy } from './ButtonCopy';
import { ButtonGoTo } from './ButtonGoTo';
import { getResults36000 } from '../utils/36000'
import { POPULATION, SUPERFICIE, COMMUNES } from '../utils/constants'

export function Table36000 (props) {

    const userData = props.userData;
    const results = getResults36000(userData.communes);
    const nbreCommunes = results[0].total;
    const hasCommune = nbreCommunes > 0;
    const nbrePercent = results[0].total ? (parseInt(results[0].total)*100 / COMMUNES).toFixed(2) : "0";
    // Source: https://www.insee.fr/fr/statistiques/1892086?sommaire=1912926
    const popPercent = results[1].total ? (parseInt(results[1].total)*100 / POPULATION).toFixed(2) : "0";
    const popAbs = results[1].total ? results[1].total.toLocaleString('fr-FR') : "0";
    const surfPercent = results[2].total ? (parseInt(results[2].total)*100 / SUPERFICIE).toFixed(2) : "0";
    const surfAbs = results[2].total ? parseFloat(parseFloat(results[2].total).toFixed(2)).toLocaleString('fr-FR') : "0";

    var display_code = "[size=150]MÀJ de "+ userData.userflag +" "+ userData.username +"[/size]\n";
    display_code += "[color=blue][b]"+nbreCommunes+"[/b][/color] communes soit [color=blue][b] "+nbrePercent+" % [/b][/color] du nombre de communes\n";
    display_code += "[color=red][b] "+popAbs+" [/b][/color] habitants soit [color=red][b] "+popPercent+" % [/b][/color] de la population\n";
    display_code += "[color=green][b] "+surfAbs+" [/b][/color] km² soit [color=green][b] "+surfPercent+" % [/b][/color] de la superficie\n";
    if (hasCommune) {
        display_code += "\n";
        display_code += "la plus peuplée : "+results[1].max_name+" avec "+results[1].max.toLocaleString('fr-FR')+" habitants\n";
        display_code += "la moins peuplée : "+results[1].min_name+" avec "+results[1].min.toLocaleString('fr-FR')+" habitants\n";
        display_code += "\n";
        display_code += "la plus étendue : "+results[2].max_name+" avec "+results[2].max.toFixed(2)+" km²\n";
        display_code += "la moins étendue : "+results[2].min_name+" avec "+results[2].min.toFixed(2)+" km²\n";
        display_code += "\n";
        display_code += "la plus élevée : "+results[3].max_name+" avec "+results[3].max.toFixed(2)+"m \n";
        display_code += "la moins élevée : "+results[3].min_name+" avec "+results[3].min.toFixed(2)+"m \n";
        display_code += "altitude moyenne : "+results[3].moyenne.toFixed(2)+"m \n";
    }
    return (
        <Table bordered>
            <thead>
                <tr><th><h4><span role="img" aria-label="flag">🇫🇷</span> 36000 communes</h4></th></tr>
            </thead>
            <tbody>
                <tr><td>
                <h5>MÀJ de {userData.userflag} {userData.username}</h5>
                <h5>
                    <b className='blue'>{results[0].total}</b> communes soit <b className='blue'>{nbrePercent}%</b> du nombre de communes<br/>
                    <b className='red'>{popAbs}</b> habitants soit <b className='red'>{popPercent}%</b> de la population<br/>
                    <b className='green'>{surfAbs}</b> km² soit <b className='green'>{surfPercent}%</b> de la superficie
                </h5>
                {hasCommune &&
                <p>
                la plus peuplée : {results[1].max_name} avec {results[1].max.toLocaleString('fr-FR')} habitants<br/>
                la moins peuplée : {results[1].min_name} avec {results[1].min.toLocaleString('fr-FR')} habitants<br/>
                <br/>
                la plus étendue : {results[2].max_name} avec {results[2].max.toFixed(2)} km²<br/>
                la moins étendue : {results[2].min_name} avec {results[2].min.toFixed(2)} km²<br/>
                <br/>
                la plus élevée : {results[3].max_name} avec {results[3].max.toFixed(2)}m<br/>
                la moins élevée : {results[3].min_name} avec {results[3].min.toFixed(2)}m<br/>
                altitude moyenne : {results[3].moyenne.toFixed(2)}m
                </p>}
                </td></tr>
                <tr className="not-public"><td><textarea name='message' id='code-forum' rows='8' cols='76' tabIndex='4' className='md-textarea form-control' defaultValue={display_code} /></td></tr>
                <tr className="not-public"><td>
                    <ButtonCopy />
                    <ButtonGoTo location='https://forum.eurobilltracker.com/posting.php?mode=reply&f=34&t=7171' />
                </td></tr>
            </tbody>
        </Table>
    );
}
