import React from 'react';

export function LinkBankNote(props) {
    return (<a href={"https://fr.eurobilltracker.com/notes/?id=" + props.noteRef} title='voir le billet sur eurobilltracker' className='lead' target='new' rel="noopener">1<sup>er</sup> <span role="img" aria-label="€">💶</span><br/><span>Eurobilltracker</span></a>)
}
