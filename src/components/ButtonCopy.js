import React from 'react';
import Button from 'react-bootstrap/Button'

export function ButtonCopy () {
    function handleClick() {
        const textarea = document.getElementById("code-forum")
        textarea.focus()
        textarea.select()
        document.execCommand('copy')
    }

    return (<Button variant="primary" id='btn-copy' href="#" onClick={handleClick}>Copier</Button>);
}
