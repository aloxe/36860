import React from 'react';
import { Link } from "react-router-dom";
import Table from 'react-bootstrap/Table';
import { timeConverter } from '../utils/strings'
import {ReactComponent as FranceIcon} from "../assets/france.svg"
import './Leaderboard.css';

export function Leaderboard(props) {

    const users = props.users.sort((a, b) => (a.nombre < b.nombre) ? 1 : ((b.nombre < a.nombre) ? -1 : 0));
    const userlist = users.filter(el => (el.username !== "anonymous") )

    return (
        <Table bordered>
            <thead>
                <tr><th colSpan='4'>
                <h3>Classement général</h3>
                </th></tr>
            </thead>
            <tbody>
                {userlist.map((user, index)  => (
                    <tr key={user.user}>
                        <td>
                            {index+1}
                        </td><td>
                            { user.type !== "novisit" ? <Link to={"/"+user.user} title="voir les communes">{user.userflag} {user.username}</Link> : <span>{user.userflag} {user.username}</span> }
                        </td><td>
                            {user.nombre}
                            <div className="small">({user.date ? timeConverter(user.date) : "ancien"})</div>
                        </td><td>
                            {user.type !== "novisit" && <a className="icon" href={"//ebt.fr.eu.org/carte.html?#?u="+user.user+"&c=blue"} title="voir sa carte">
                                <FranceIcon height="18px" width="18px" alt="carte de France" />
                            </a>}
                        </td>
                    </tr>
                ))}
            </tbody>
        </Table>
    )
}
