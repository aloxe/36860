import React from 'react'
import { Link } from "react-router-dom"
import Meta from './Meta';
import './Splash.css'

export function Splash () {

    return (
        <>
            <Meta
                schema="AboutPage"
                title="EBT Carte de France, la carte de France pour Eurobilltracker"
                description="Encodez des billets en France, Envoyez les données ici et découvrez dans combien de communes vous avez trouvé des billets avec une belle carte détaillée."
                contentType="website"
            />
            <ol>
                <li><h2><a href="https://fr.eurobilltracker.com/new/">Track €banknotes</a> everywhere in France.</h2>
                <span className="arrow">➠</span></li>
                <li><h2><Link to="/form">Upload your €banknotes data here</Link>.</h2>
                <span className="arrow">➠</span></li>
                <li><h2>See your map and much more.</h2>
                <span className="arrow">➠</span></li>
                <li><h2><a href="https://forum.eurobilltracker.com/viewtopic.php?f=34&t=7171">Challenge others</a>.</h2></li>
            </ol>
        </>
    );
}
