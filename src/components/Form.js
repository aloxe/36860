import React from 'react';
import { Link } from "react-router-dom";
import { Spinner } from 'react-bootstrap'
import { MessageLog } from './MessageLog';
import { CSVDropReader } from './CSVDropReader';
import Menu from './Menu';
import Meta from './Meta';

export default class Form extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            messages: [""],
        }
        this.handleMessage = this.handleMessage.bind(this)
    }

    handleMessage(key, value) {
        if (document.getElementById('messages_inside')) {
            let newMessages = this.state.messages ? this.state.messages : [ "" ];
            newMessages.push(value);
            this.setState({[key]: newMessages},()=> {
                document.getElementById('messages_inside').scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"})
            })
        }
    }
    componentDidMount() {
        const userLoading = {userData: true, userFlawData: true}
        this.props.stateHandler("userLoading", userLoading)
    }

    componentWillUnmount() {
        if (this.props.match.params.id) {
            const userLoading = {userData: true, userFlawData: true }
            const dataUpoading = { dataUser: true, dataMemory: true, dataCommunes: true, dataUnknown: true }
            this.props.stateHandler("userLoading", userLoading)
            this.props.stateHandler("dataUpoading", dataUpoading)
        }
    }

    render() {
        const stillLoading = allTrue(this.props.state.loading);
        const stillDataUpoading = allTrue(this.props.state.dataUpoading);
        const { loading, user } = this.props.state;

        return (
            <>
                <Meta
                    schema="WebPage"
                    title="Envoyez la liste de vos billets"
                    description="Envoyez la liste de vos billets entrés sur Eurobilltracker pour voir votre parcours en France"
                    path={window.location.pathname.substring(1)}
                    contentType="form"
                />
                <MessageLog messages={this.state.messages} loading={loading} />
                <Link to="/list" className="upload">Classement Général</Link>

                {stillLoading &&<Spinner animation="border" role="status" size="lg"
                    variant="primary"><span className="sr-only">Loading...</span></Spinner>}

                {!stillLoading && stillDataUpoading && <CSVDropReader stillLoading={stillLoading} state={this.props.state} stateHandler={this.props.stateHandler} handleMessage={this.handleMessage} />}

                {!stillDataUpoading && user.name && <Menu state={this.props.state} stateHandler={this.props.stateHandler}  handleMessage={this.handleMessage} reqUser={this.props.state.user} />}
            </>
        );
    }
}

function allTrue(obj) {
    for(var o in obj)
        if(obj[o]) return true;
    return false;
}
