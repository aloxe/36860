import React from 'react';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import { CarteSvg } from "./CarteSvg"
import { ButtonCopy } from './ButtonCopy';
import { ButtonGoTo } from './ButtonGoTo';


export function TableTourdeFrance (props) {
    const userData = props.userData;
    const visitedDepartements = getResultsTourDeFrance(userData.communes, props.departementsFrancais)[0]

    function handleOnDownload() {
        var img = new Image();
        img.onload = function (){
            var canvas = document.createElement("canvas");
            canvas.width = img.naturalWidth;
            canvas.height = img.naturalHeight;
            var ctxt = canvas.getContext("2d");
            ctxt.fillStyle = "#fff";
            ctxt.fillRect(0, 0, canvas.width, canvas.height);
            ctxt.drawImage(img, 0, 0);
            var a = document.createElement("a");
            a.href = canvas.toDataURL("image/png");
            a.download = "CARTE_DE_FRANCE.png"
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
        };
        var innerSvg = document.querySelector("svg");
        for(let i = 0; i < innerSvg.children.length; i++) {
            if (visitedDepartements.includes(innerSvg.children[i].id))
                innerSvg.children[i].setAttribute("fill", "crimson")
            else
                innerSvg.children[i].setAttribute("fill", "navy")
        }
        var svgText = (new XMLSerializer()).serializeToString(innerSvg);
        img.src = "data:image/svg+xml;utf8," + encodeURIComponent(svgText);
    }

    function getResultsTourDeFrance(visited, departementsFrancais) {
        var visitedDepartements = [];
        var visitedTourDeFrance = [];
        for (var i=0; i<visited.length; i++) {
            var visitCode = visited[i].codeCommune.substr(0, 2).toUpperCase();
            if (!visitedDepartements.includes(visitCode))
                for (var j=0;j<departementsFrancais.length;j++) {
                    if (departementsFrancais[j].departmentCode.toUpperCase() === visitCode)
                    {
                        visitedTourDeFrance.push( { departementCode: visitCode, regionName: departementsFrancais[j].regionName });
                        visitedDepartements.push(visitCode);
                    }
                }
        }
        visitedTourDeFrance = visitedTourDeFrance.sort((a, b) => (a.departementCode > b.departementCode) ? 1 : ((b.departementCode > a.departementCode) ? -1 : 0));
        visitedTourDeFrance = visitedTourDeFrance.sort((a, b) => (a.regionName > b.regionName) ? 1 : ((b.regionName > a.regionName) ? -1 : 0));
        return [visitedDepartements, visitedTourDeFrance];
    }

    const nbreVisitedDpt = (regionName, visitedDepartements, allRegions) => {
        const regionNamed = allRegions.find(region => region.nom === regionName);
        const visitedDepartementsInRegion =
            regionNamed.departements.filter(departement => visitedDepartements.includes(departement));
        return visitedDepartementsInRegion.length
    }

    const AllDep = (regionName, visitedDepartements, allRegions) => {
        const regionNamed = allRegions.find(region => region.nom === regionName);
        const visitedDepartementsInRegion =
            regionNamed.departements.filter(departement => visitedDepartements.includes(departement));
        return visitedDepartementsInRegion.length === regionNamed.nombreDep
    }

    const displayCode = (userData, visitedDepartements, allRegions) => (
        "[size=150]MÀJ de "+props.userData.userflag+" "+props.userData.username+"[/size]\n"+
        allRegions.map(region => (
            AllDep(region.nom, visitedDepartements, allRegions) ? "[b]"+region.nom+"[/b]" : region.nom
            +" ("+nbreVisitedDpt(region.nom, visitedDepartements, allRegions)+"/"+region.departements.length+") :\n"+
            region.departements.map(departement => (
                visitedDepartements.includes(departement)
                ? "[color='forestgreen'][b]"+departement+"[/b][/color] \n"
                : "[color='lightslategray']"+departement+"[/color] \n"
            )).join('')
        )+"\n\n").join('')
    )

    return (
        <Table bordered>
            <thead>
                <tr><th><h4><span role="img" aria-label="vélo">🚴</span> Tour de France</h4></th></tr>
            </thead>
            <tbody>
                <tr><td>
                <h5>MÀJ de {userData.userflag} {userData.username}</h5>
                {allRegions.map(region => (
                    <div key={region.nom} className={AllDep(region.nom, visitedDepartements, allRegions) ? "bold" : ""}>
                        <span>
                            {region.nom} ({nbreVisitedDpt(region.nom, visitedDepartements, allRegions)}/{region.departements.length}) :
                        </span>
                        {region.departements.map(departement => (
                            <span key={departement}
                                className={visitedDepartements.includes(departement) ? "forestgreen" : "lightslategray"}>
                                 {departement}
                            </span>
                        ))}
                    </div>
                ))}
                </td></tr>
                <tr><td>
                    <CarteSvg visitedDepartements={visitedDepartements} width="400px"/>
                </td></tr>
            <tr className="not-public"><td><textarea name='message' id='code-forum' rows='8' cols='76' tabIndex='4' className='md-textarea form-control' defaultValue={displayCode(userData, visitedDepartements, allRegions)} /></td></tr>
                <tr className="not-public"><td>
                    <Button variant="outline-info" onClick={handleOnDownload}>Télécharger la carte</Button>
                    <ButtonCopy  variant="outline-primary" />
                    <ButtonGoTo variant="outline-primary" location='https://forum.eurobilltracker.com/posting.php?mode=reply&f=34&t=6255' />
                </td></tr>
            </tbody>
        </Table>
    );
}

var allRegions = [
    {
        nom: "Auvergne-Rhône-Alpes",
        nombreDep: "12",
        departements: ["01", "03", "07", "15", "26", "38", "42", "43", "63", "69", "73", "74"]
    },
    {
        nom: "Bourgogne-Franche-Comté",
        nombreDep: "8",
        departements: ["21", "25", "39", "58", "70", "71", "89", "90"]
    },
    {
        nom: "Bretagne",
        nombreDep: "4",
        departements: ["22", "29", "35", "56"]
    },
    {
        nom: "Centre-Val de Loire",
        nombreDep: "6",
        departements: ["18", "28", "36", "37", "41", "45"]
    },
    {
        nom: "Corse",
        nombreDep: "2",
        departements: ["2A", "2B"]
    },
    {
        nom: "Grand Est",
        nombreDep: "10",
        departements: ["08", "10", "51", "52", "54", "55", "57", "67", "68", "88"]
    },
    {
        nom: "Hauts-de-France",
        nombreDep: "5",
        departements: ["02", "59", "60", "62", "80"]
    },
    {
        nom: "Île-de-France",
        nombreDep: "8",
        departements: ["75", "77", "78", "91", "92", "93", "94", "95"]
    },
    {
        nom: "Normandie",
        nombreDep: "5",
        departements: ["14", "27", "50", "61", "76"]
    },
    {
        nom: "Nouvelle-Aquitaine",
        nombreDep: "12",
        departements: ["16", "17", "19", "23", "24", "33", "40", "47", "64", "79", "86", "87"]
    },
    {
        nom: "Occitanie",
        nombreDep: "13",
        departements: ["09", "11", "12", "30", "31", "32", "34", "46", "48", "65", "66", "81", "82"]
    },
    {
        nom: "Pays de la Loire",
        nombreDep: "5",
        departements: ["44", "49", "53", "72", "85"]
    },
    {
        nom: "Provence-Alpes-Côte d'Azur",
        nombreDep: "6",
        departements: ["04", "05", "06", "13", "83", "84"]
    }
];
