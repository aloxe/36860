import React from 'react';
import { Link } from "react-router-dom";
import { Leaderboard } from './Leaderboard';
import { LeaderboardForum } from './LeaderboardForum';
import Meta from './Meta';
import './Home.css'


export default class Home extends React.Component {

    componentDidMount() {
        const userLoading = {userData: true, userFlawData: true}
        this.props.stateHandler("userLoading", userLoading)
    }

    componentWillUnmount() {
        if (this.props.match.params.id) {
            const userLoading = {userData: true, userFlawData: true }
            const dataUpoading = { dataUser: true, dataMemory: true, dataCommunes: true, dataUnknown: true }
            this.props.stateHandler("userLoading", userLoading)
            this.props.stateHandler("dataUpoading", dataUpoading)
        }
    }

    render() {
        const { user } = this.props.state;
        return (
            <>
                <Meta
                    schema="WebPage"
                    title="Classement Général des meilleurs trackers de France"
                    description="Classement Général des meilleurs encodeurs Eurobilltracker par communes visitées en  France."
                    path={window.location.pathname.substring(1)}
                    contentType="form"
                />
                <Link to="/form" className="upload">Envoyez vos données</Link>
                {<div ClassName="public"><Link to="/list" className="upload not-public">Leaderboard</Link></div>}
                {<div ClassName="public"><Link to="/listforum" className="upload not-public">Classement pour le forum</Link></div>}
                {this.props.match.path === "/list" && <Leaderboard key={window.location.pathname}  users={this.props.state.leaderboard} user={user} />}
                {this.props.match.path === "/listforum" && <LeaderboardForum key={window.location.pathname}  users={this.props.state.leaderboard} user={user} />}
            </>
        );
    }
}
