import React from 'react';
import Table from 'react-bootstrap/Table';
import { ButtonCopy } from './ButtonCopy';
import { ButtonGoTo } from './ButtonGoTo';


export function TablePrefectures (props) {

    const userData = props.userData;
    const results = getResultsPrefectures(userData.communes, props.departementsFrancais);
    const prefectures = results.sort((a, b) => (a.departementCode > b.departementCode) ? 1 : ((b.departementCode > a.departementCode) ? -1 : 0));

    var display_code = "[size=150]MÀJ de "+userData.userflag+" "+userData.username+" : "+ prefectures.length + " préfectures.[/size]\n";
    for (var i=0; i<prefectures.length; i++)
        display_code += "[color=lightslategray][b]"+prefectures[i].departementCode+"[/b][/color] "+prefectures[i].nomCommune+"\n";

    return (
        <Table bordered>
            <thead>
                <tr><th><h4><span role="img" aria-label="prefecture">🏛</span> Jeu des préfectures</h4></th></tr>
            </thead>
            <tbody>
                <tr><td>
                <h5>MÀJ de {userData.userflag} {userData.username}</h5>
                <p>
                {prefectures.map(prefecture => (
                    <span key={prefecture.departementCode}>
                        <b className="lightslategray" >{prefecture.departementCode}</b>
                         {prefecture.nomCommune}<br/>
                    </span>
                ))}
                </p>
                </td></tr>
                <tr className="not-public"><td><textarea name='message' id='code-forum' rows='8' cols='76' tabIndex='4' className='md-textarea form-control' defaultValue={display_code} /></td></tr>
                <tr className="not-public"><td>
                    <ButtonCopy />
                    <ButtonGoTo location='https://forum.eurobilltracker.com/posting.php?mode=reply&f=34&t=11477' />
                </td></tr>
            </tbody>
        </Table>
    );
}

function getResultsPrefectures(visited, departementsFrancais) {
    var allPrefectures = [];
    var visitedPrefectures = [];
    for (var i=0; i<departementsFrancais.length; i++) {
        allPrefectures.push(departementsFrancais[i].departmentCode.toUpperCase()+" "+departementsFrancais[i].prefecture);
    }
    for (var j=0; j<visited.length; j++) {
        let code = visited[j].codeCommune.substr(0, 2).toUpperCase();
        let nom = visited[j].nomCommune;
        if (allPrefectures.includes(code+" "+nom))
            visitedPrefectures.push({ departementCode: code, nomCommune: nom })
    }
    return visitedPrefectures;
}
