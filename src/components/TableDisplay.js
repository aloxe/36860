import React from 'react';
import { Table36000 } from './Table36000';
import { TablePrefectures } from './TablePrefectures';
import { TableTourdeFrance } from './TableTourdeFrance';
import { TableTableau } from './TableTableau';
import { TableListe } from './TableListe';
import { TableErrors } from './TableErrors';
import './TableDisplay.css';

export default class TableDisplay extends React.Component {

    render(props) {
        const appState = this.props.state
        const userData = this.props.state.userData;
        const departementsFrancais = this.props.state.departementsFrancais;
        const tableName = this.props.tableRequested;
        const stateHandler = this.props.stateHandler;
        const handleMessage = this.props.handleMessage;

        function SwitchTable(props) {
            switch(props.children) {
                case 'erreurs':
                    return <TableErrors userData={userData} state={appState}
                        stateHandler={stateHandler} handleMessage={handleMessage} />
                case 'doublons':
                    return <TableErrors type="doublons" state={appState}
                        stateHandler={stateHandler} handleMessage={handleMessage} />;
                case 'tableau':
                    return <TableTableau userData={userData} departementsFrancais={departementsFrancais}/>;
                case 'liste':
                    return <TableListe userData={userData}/>;
                case 'communes':
                    return <Table36000 userData={userData}/>;
                case 'tourdefrance':
                    return <TableTourdeFrance userData={userData} departementsFrancais={departementsFrancais}/>;
                case 'prefectures':
                    return <TablePrefectures userData={userData} departementsFrancais={departementsFrancais}/>;
                default:
                    return null;
            }
        }

        return (
            <div className="row center block">
                <SwitchTable>{tableName}</SwitchTable>
            </div>
        );
    }
}
