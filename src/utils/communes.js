import String from '../utils/strings'

///////////////////
// data manipulations with communes and location data
///////////////////

/*
extracts communes from the billets.csv ebt files
@params data: json data as input
returns an array of valid communes, invalid locations
*/
export function getExtractCommunes(data, state){
    var visitedLocations = []; // list of visited locations
    var visited = []; // the list of visited communes
    var visitedCode = []; // list of unique codes for visited communes
    var visitedUnknown = []; // list locations with no or several match
    var checkedCommune = {};
    var communeSansCodePostal = null;

    for (var i=0; i < data.length; i++) {
        var cells = data[i].data;
        if (cells[6] === "France")
        {
            var visitedLocation = cells[7]+" "+cells[5];
            if (!visitedLocations.includes(visitedLocation))
            {
                visitedLocations.push(visitedLocation);
                if (cells[7].match(/\b[0-9]{5}\b/))
                {
                    checkedCommune = checkCommune(cells[5], cells[7], state.codesPostaux);
                    if (checkedCommune === null)
                    {
                        checkedCommune = checkEBTCommune(cells[5], cells[7], state.ebtNames);
                    }
                    if (checkedCommune === null)
                    {
                        var checkedAproxCommune = checkAproxCommune(cells[5], cells[7], state.codesPostaux);
                        if (checkedAproxCommune.length === 1)
                        {
                            checkedCommune = checkedAproxCommune[0];
                            checkedCommune.ref = cells[9];
                        }
                        else if (checkedAproxCommune.length > 0)
                        {
                            // Enregistrement multiple choice
                            visitedUnknown.push({
                                type: "multiple",
                                location: {nomCommune:cells[5],codePostal:cells[7],codeCommune:null},
                                map: {lat:cells[12],long:cells[13]},
                                possibleCommunes: checkedAproxCommune,
                                note: cells[0],
                                ref: cells[9]
                            });
                        }
                    }
                    if (checkedCommune !== null)
                    {
                        if (visitedCode.includes(checkedCommune.codeCommune))
                        {
                            var possibleCommune = findVisitedCommuneByInsee(checkedCommune.codeCommune, visited);
                            // if same name: it's just that the municipality has more than one postal code.
                            // if not: we record as a duplicate
                            if (possibleCommune.nomCommune !== cells[5]) {
                                visitedUnknown.push({
                                    type: "double",
                                    location: {nomCommune:cells[5],codePostal:cells[7],codeCommune:null},
                                    possibleCommune: possibleCommune,
                                    map: {lat:cells[12],long:cells[13]},
                                    note: cells[0],
                                    ref: cells[9]
                                });
                            }
                        }
                        else
                        {
                            checkedCommune.ref = cells[9];
                            visited.push(checkedCommune);
                            visitedCode.push(checkedCommune.codeCommune);
                        }
                    }
                    else if (checkedAproxCommune === null || checkedAproxCommune.length === 0)
                    {
                        communeSansCodePostal = checkCommuneSansCodePostal(cells[5], cells[7], state.codesPostaux);
                        if (communeSansCodePostal)
                        {
                            // Enregistrement possible wrong postcode
                            visitedUnknown.push({
                                type: "wrong",
                                location: {nomCommune:cells[5],codePostal:cells[7],codeCommune:null},
                                map: {lat:cells[12],long:cells[13]},
                                note: cells[0],
                                ref: cells[9]
                            });
                        }
                        else {
                            // Enregistrement unknown
                            visitedUnknown.push({
                                type: "unknown",
                                location: {nomCommune:cells[5],codePostal:cells[7],codeCommune:null},
                                map: {lat:cells[12],long:cells[13]},
                                note: cells[0],
                                ref: cells[9]
                            });
                        }
                    }
                }
                else
                {
                    // Enregistrement postcode malformed
                    visitedUnknown.push({
                        type: "bad",
                        location: {nomCommune:cells[5],codePostal:cells[7],codeCommune:null},
                        map: {lat:cells[12],long:cells[13]},
                        note: cells[0],
                        ref: cells[9]
                    });
                }
            }
        }
    }

    visited = groupParisLyonMarseille(visited)
    visited = addPopulation(visited, state.population)
    visited = addSuperficie(visited, state.superficie)
    visited = addAltitude(visited, state.altitude)
    return [visited, visitedUnknown]
}

/////////////////
// Check if location name is a commune
/////////////////
function checkCommune(nomCommune, codePostal, codesPostaux) {
    for (var i=0; i<codesPostaux.length; i++) {
        var checkingCommune = {
            codePostal: codesPostaux[i].codePostal,
            codeCommune:  codesPostaux[i].codeCommune,
            nomCommune: codesPostaux[i].nomCommune
        };
        if (codesPostaux[i].codePostal === codePostal)
        {
            if (codesPostaux[i].nomCommune.toLowerCase() === nomCommune.toLowerCase())
                return checkingCommune;
            else if (codesPostaux[i].nomCommune.toLowerCase().replace(/-/g, " ") === nomCommune.toLowerCase().replace(/-/g, " "))
                return checkingCommune;
            else if (codesPostaux[i].nomCommune.sansAccent().toLowerCase() === nomCommune.sansAccent().toLowerCase())
                return checkingCommune;
            else if (codesPostaux[i].nomCommune.sansAccent().toLowerCase().replace(/-/g, " ") === nomCommune.sansAccent().toLowerCase().replace(/-/g, " "))
                return checkingCommune;
        }
    }
    return null;
}

export function checkCommuneSansCodePostal(nomCommune, codePostal, codesPostaux) {
    for (var i=0; i<codesPostaux.length; i++) {
        var checkingCommune = {
            codePostal: codesPostaux[i].codePostal,
            codeCommune:  codesPostaux[i].codeCommune,
            nomCommune: codesPostaux[i].nomCommune
        };
        if (codesPostaux[i].codePostal.substr(0,2) === codePostal.substr(0,2))
        {
            if (codesPostaux[i].nomCommune.toLowerCase() === nomCommune.toLowerCase())
                return checkingCommune;
            else if (codesPostaux[i].nomCommune.toLowerCase().replace(/-/g, " ") === nomCommune.toLowerCase().replace(/-/g, " "))
                return checkingCommune;
            else if (codesPostaux[i].nomCommune.sansAccent().toLowerCase() === nomCommune.sansAccent().toLowerCase())
                return checkingCommune;
            else if (codesPostaux[i].nomCommune.sansAccent().toLowerCase().replace(/-/g, " ") === nomCommune.sansAccent().toLowerCase().replace(/-/g, " "))
                return checkingCommune;
        }
    }
    return null;
}

function checkEBTCommune(nomCommune, codePostal, ebtNames) {
    for (var i=0; i<ebtNames.lieux.length; i++) {
        var checkingCommune = {
            codePostal: ebtNames.lieux[i].codePostal,
            codeCommune:  ebtNames.lieux[i].codeCommune,
            nomCommune: ebtNames.lieux[i].nomCommune
        };
        if (ebtNames.lieux[i].codePostal === codePostal)
            if (ebtNames.lieux[i].nomEBT === nomCommune)
                return checkingCommune;
    }
    return null;
}

function checkAproxCommune(nomCommune, codePostal, codesPostaux) {
    var checkedAproxCommune = [];
    for (var i=0; i<codesPostaux.length; i++) {
        var checkingCommune = {
            codePostal: codesPostaux[i].codePostal,
            codeCommune:  codesPostaux[i].codeCommune,
            nomCommune: codesPostaux[i].nomCommune
        };
        if (codesPostaux[i].codePostal === codePostal)
            if (codesPostaux[i].nomCommune.includes(nomCommune))
                checkedAproxCommune.push(checkingCommune);
            else if (codesPostaux[i].nomCommune.sansAccent().includes(nomCommune.sansAccent()))
                checkedAproxCommune.push(checkingCommune);
    }
    return checkedAproxCommune;
}

/////////////////
// Get commune details from other data
////////////////
function findVisitedCommuneByInsee(codeCommune, visited) {
    for (var i=0; i < visited.length; i++) {
        if (visited[i].codeCommune === codeCommune)
            return visited[i];
    }
    return null;
}

export function getCommuneByInsee(codeCommune, codesPostaux) {
    for (var i=0; i < codesPostaux.length; i++) {
        if (codesPostaux[i].codeCommune === codeCommune) {
            delete codesPostaux[i].libelleAcheminement
            return codesPostaux[i];
        }
    }
    return null;
}

export function allCommunesByCodePostal(codePostal, codesPostaux) {
    var list = [];
    for (var i=0; i<codesPostaux.length; i++) {
        var checkingCommune = {
            codePostal: codesPostaux[i].codePostal,
            codeCommune:  codesPostaux[i].codeCommune,
            nomCommune: codesPostaux[i].nomCommune
        };
        if (checkingCommune.codePostal === codePostal)
            list.push(checkingCommune);
    }
    return list;
}

/*
36000
 - parsing EBT communes results to fit 36000 game
 - load population, area and altitude
 - add these tata to each location
 - return results (min, max, total)
*/
export function groupParisLyonMarseille(visited) {
    var visitedPLM = [];
    visited.sort((a,b) => (a.codePostal > b.codePostal) ? 1 : ((b.codePostal > a.codePostal) ? -1 : 0));
    for (var i=0; i<visited.length; i++) {
        if (visited[i].codeCommune > 75100 && visited[i].codeCommune <= 75120)
            var paris = {
                "codePostal": "75000",
                "codeCommune": "75056",
                "nomCommune": "Paris",
                "ref": visited.ref
            };
        else if (visited[i].codeCommune > 69380 && visited[i].codeCommune <= 69389)
            var lyon = {
                "codePostal": "69000",
                "codeCommune": "69123",
                "nomCommune": "Lyon",
                "ref": visited.ref
            };
        else if (visited[i].codeCommune > 13200 && visited[i].codeCommune <= 13216)
            var marseille = {
                "codePostal": "13000",
                "codeCommune": "13055",
                "nomCommune": "Marseille",
                "ref": visited.ref
            };
        else
            visitedPLM.push(visited[i]);
        }
    if (paris)
        visitedPLM.push(paris);
    if (lyon)
        visitedPLM.push(lyon);
    if (marseille)
        visitedPLM.push(marseille);

    return visitedPLM;
}

export function getPopulation(codeCommune, population) {
    for (var i=0; i<population.length; i++) {
        let code = population[i]["Code département"] + population[i]["Code commune"];
        if (code === codeCommune)
            return parseInt(population[i]["Population totale"].replace(/\s/g, ""));
    }
    return 0;
}

export function getSuperficie(codeCommune, superficie) {
    for (var i=0; i<superficie.length; i++) {
        if (superficie[i].codeCommune === codeCommune)
            return parseFloat(superficie[i].surface);
    }
    return 0;
}

export function getAltitude(codeCommune, altitude) {
    for (var i=0; i<getAltitude.length; i++) {
        if (altitude[i]["Code INSEE"] === codeCommune)
            return parseFloat(altitude[i]["Altitude Moyenne"]);
    }
    return 0;
}

function addPopulation(visited, population) {
    var visitedPop = [];
    var code = "";
    var name = "";
    var pop = 0;
    for (var i=0; i<population.length; i++) {
        code = population[i]["Code département"] + population[i]["Code commune"];
        name = population[i]["Nom de la commune"];
        pop = parseInt(population[i]["Population totale"].replace(/\s/g, ""));

        for (var j=0; j<visited.length; j++) {
            if (visited[j].codeCommune === code)
            {
                visited[j].population = pop;
                if (visited[j].nomCommune !== name)
                    visited[j].population_note = "From " + name;
                visitedPop.push(visited[j]);
            }
        }
    }
    for (var k=0; k<visited.length; k++) {
        if (!visited[k].population)
        {
            visited[k].population = "0";
            visited[k].population_note = "notfound";
            visitedPop.push(visited[k]);
        }
    }
    return visitedPop;
}

export function addSuperficie(visited, superficie) {
    var visitedSurf = [];
    var code = "";
    var name = "";
    var surf = 0;
    for (var i=0; i<superficie.length; i++) {
        code = superficie[i].codeCommune;
        name = superficie[i].nom;
        surf = parseFloat(superficie[i].surface);
        for (var j=0; j<visited.length; j++) {
            if (visited[j].codeCommune === code)
            {
                visited[j].superficie = surf;
                if (visited[j].nomCommune !== name)
                    visited[j].superficie_note = "From " + name;
                visitedSurf.push(visited[j]);
            }
        }
    }
    for (var k=0; k<visited.length; k++) {
        if (!visited[k].superficie)
        {
            visited[k].superficie = "0";
            visited[k].superficie_note = "notfound";
            visitedSurf.push(visited[k]);
        }
    }
    return visitedSurf;
}

export function addAltitude(visited, altitude) {
    var visitedAlt = [];
    var code = "";
    var alt = 0;
    for (var i=0; i<altitude.length; i++) {
        code = altitude[i]["Code INSEE"];
        alt = parseFloat(altitude[i]["Altitude Moyenne"]);
        for (var j=0; j<visited.length; j++) {
            if (visited[j].codeCommune === code)
            {
                visited[j].altitude = alt;
                visitedAlt.push(visited[j]);
            }
        }
    }
    for (var k=0; k<visited.length; k++) {
        if (!visited[k].altitude)
        {
            visited[k].altitude_note = "notfound";
            visitedAlt.push(visited[k]);
        }
    }
    return visitedAlt;
}
