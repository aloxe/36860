export function setCookie(name, value, daysToLive) {
    // Encode value in order to escape semicolons, commas, and whitespace
    // var encodeVal = btoa(encodeURIComponent(value))
    var cookie = name + "=" + btoa(value);

    if(typeof daysToLive === "number") {
        cookie += "; max-age=" + (daysToLive*24*60*60);
        document.cookie = cookie;
    }
}

export function getCookie(name) {
    var cookieArr = document.cookie.split(";");
    for(var i = 0; i < cookieArr.length; i++) {
        var cookiePair = cookieArr[i].split("=");

        /* Removing whitespace at the beginning of the cookie name
        and compare it with the given string */
        if(name === cookiePair[0].trim())
            return atob(cookiePair[1]);
    }
    return null;
}

//decodeURIComponent(
