export const POPULATION = 66732538
export const POPULATION_META = {
    year: "2018",
    source: "https://www.vie-publique.fr/en-bref/277901-recensement-et-populations-legales-les-chiffres-2021"
}
export const SUPERFICIE = 543939.9
export const SUPERFICIE_META = {
    year: "2017",
    source: "https://www.insee.fr/fr/statistiques/1405599?geo=METRO-1",
    other: "https://www.insee.fr/fr/statistiques/1405599?geo=FE-1"
}
export const COMMUNES = 35227
export const COMMUNES_META = {
    year: "2019",
    source: "https://fr.wikipedia.org/wiki/Nombre_de_communes_en_France#Ann%C3%A9e_2019"
}
