// string without diacritics
String.prototype.sansAccent = function() {
    var accent = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, // N, n
        /[\307]/g, /[\347]/g, // C, c
        /[\570]/g, /[\377]/g, // Y, y
    ];
    var noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c', 'Y', 'y'];

    var str = this;
    for (var i = 0; i < accent.length; i++) {
        str = str.replace(accent[i], noaccent[i]);
    }
    str = str.replace("œ", "oe");
    str = str.replace("æ", "ae");
    str = str.replace("Œ", "OE");
    str = str.replace("Æ", "AE");
    return str;
}

export function countryCodeToFlag(countrycode) {
    const offset = 127397;
    const A = 65;
    const Z = 90;
    const f = countrycode.codePointAt(0);
    const s = countrycode.codePointAt(1);

    if (countrycode.length !== 2
        || f > Z || f < A
        || s > Z || s < A)
        throw new Error('Not an alpha2 country code');

    return String.fromCodePoint(f + offset)+String.fromCodePoint(s + offset);
}

export function timeConverter(UNIX_timestamp) {
    const a = new Date(UNIX_timestamp * 1000);
    const months = ['Janvier','Février','Mars','Avril','Mai','Juin','Jullet','Aout','Septembre','Octobre','Novembre','Décembre'];

    return a.getDate() + ' ' + months[a.getMonth()] + ' ' + a.getFullYear();
}
