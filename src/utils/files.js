import { countryCodeToFlag } from '../utils/strings'
import { getCookie } from '../utils/cookies'

export function addLeaderToBoard(leader, leaderboard) {
    let newLeaderBoard = leaderboard.filter(el => (el.user !== leader.user) )
    newLeaderBoard.push(leader);
    return newLeaderBoard;
}

export function uploadJson(data, handleMessage) {
    const options = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' }
    }

    fetch("/api/upload.php", options)
    .then(response => response.json())
    .then(data => {
        if (data.status === 201)
            handleMessage("message", "💾 Données " + data.type + " sauvegardées")
        else
            handleMessage("message", "💢💢 Données " + data.type + " PAS SAUVÉES")
    })
    .catch(error => {
        console.error(error)
        handleMessage("message", "💢💢 visites " + data.type + " Pas sauvegardées : " + error)
    });
}

export function getUserFromId(userId, stateHandler, handleMessage) {
    const options = {
        method: 'POST',
        body: JSON.stringify({ user: userId }),
        headers: { 'Content-Type': 'application/json' }
    }

    fetch("/api/geodata/get_ebtuser_name_country.php", options)
    .then(response => response.json())
    .then(data => {
        var newUser = {}
        if (data.status === 200)
            newUser = {
                id: userId,
                name: data.username,
                flag: countryCodeToFlag(data.userflag),
                logged: getCookie("user") && getCookie("user") === userId
            }
        else
            newUser = { id: "x"+userId+"x", name: "ANONYME", flag: "⚐", logged: false }
        stateHandler("user", newUser)
        handleMessage("message", "👤 Utilisateur identifié: " + newUser.flag +" "+ data.username)
    })
    .catch(error => console.error("get_ebtuser_name", error))
}
