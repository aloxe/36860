////////////////
// calcul des données pour le jeu des 36000 communes de france
////////////////

export function getResults36000(visited) {
    var nombre = { total:visited.length }
    var population = { total:0 };
    var superficie = { total:0 };
    var altitude = { total:0, records: 0, moyenne: 0 };
    for (var i=0; i<visited.length; i++) {
        population.total += parseInt(visited[i].population);
        if (!population.min || visited[i].population < population.min)
        {
            population.min = visited[i].population;
            population.min_name = visited[i].nomCommune;
        }
        else if (!population.max || visited[i].population > population.max)
        {
            population.max = visited[i].population;
            population.max_name = visited[i].nomCommune;
        }

        superficie.total += parseFloat(visited[i].superficie);
        if (!superficie.min || visited[i].superficie < superficie.min)
        {
            superficie.min = visited[i].superficie;
            superficie.min_name = visited[i].nomCommune;
        }
        else if (!superficie.max || visited[i].superficie > superficie.max)
        {
            superficie.max = visited[i].superficie;
            superficie.max_name = visited[i].nomCommune;
        }

        if (visited[i].altitude)
        {
            altitude.total += parseFloat(visited[i].altitude);
            altitude.records += 1;
            if (!altitude.min || visited[i].altitude < altitude.min)
            {
                altitude.min = visited[i].altitude;
                altitude.min_name = visited[i].nomCommune;
            }
            else if (!altitude.max || visited[i].altitude > altitude.max)
            {
                altitude.max = visited[i].altitude;
                altitude.max_name = visited[i].nomCommune;
            }
        }
    }
    altitude.moyenne = altitude.total / altitude.records;
    superficie.total = parseFloat(superficie.total.toFixed(2));
    return [nombre, population, superficie, altitude];
}
