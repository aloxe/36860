/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 ** in node_modules/react-dev-utils/
 */

'use strict';

const path = require('path');
const escape = require('escape-string-regexp');

module.exports = function ignoredFiles(appSrc) {
    const RegExp1 = new RegExp(
      `^(?!${escape(
        path.normalize(appSrc + '/').replace(/[\\]+/g, '/')
      )}).+/node_modules/`,
      'g'
    );
    const RegExp2 = new RegExp(
      `^(?!${escape(
        path.normalize(appSrc + '/').replace(/[\\]+/g, '/')
      )}).+/public/api/`,
      'g'
    );
      return [RegExp1, RegExp2];
};
