<?php
$json = json_decode(file_get_contents("php://input"));

if (isset($json->user))
    $user = $json->user;
if (isset($json->username))
    $username = $json->username;
if (isset($json->type))
    $type = $json->type;
else if (isset($json[0]) && isset($json[0]->type))
    $type = $json[0]->type;


if (isset($type) && $type == "names") {
    $filename = "uploads/geodata/ebt_names_communes_france.json";
    $filetype = "noms EBT ";
} else if (isset($type) && $type == "unknown") {
    $filename = "uploads/visited-".$user."-unknown.json";
    $filetype = "lieux inconnus de ".$username;
} else if (isset($type) && $type == "communes") {
    $filename = "uploads/visited-".$user."-communes.json";
    $filetype = "communes de ".$username;
} else if (isset($type) && $type == ("novisit" || "visites")) {
    $filename = "uploads/leaderboard_users.json";
    $filetype = "classement";
} else {
    $filename = "uploads/saved-".$user."-".$type.".json";
    $filetype = "FICHIER ".$user;
}

$encodedJSON = json_encode($json, JSON_PRETTY_PRINT);
$fp = fopen($filename, "w") or die("Données non écrites");
$write = fwrite($fp, $encodedJSON);

if (isset($write))
    $data = array('status' => 201, 'message' => 'File written \o/ ('.$filename.')', 'type' => $filetype);
else
    $data = array("status"=> '0', "message"=> 'File NOT written :o( ');
echo json_encode($data);

fclose($fp);
chmod($filename, 0777);
?>
