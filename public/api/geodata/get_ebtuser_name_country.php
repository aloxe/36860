<?php
// header('Access-Control-Allow-Origin: *');
// header("Access-Control-Allow-Credentials", "true");
// header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST");
// header("Access-Control-Allow-Headers", "application/json");
header("Cache-Control: no-cache");
header("Pragma: no-cache");
header("Content-Type: application/json");

$json = json_decode(file_get_contents("php://input"));
$user = $json->user;

$url = "https://fr.eurobilltracker.com/profile/?command=2048;tab=6;user=".$user;

$usernameandcountry = page_ebtname($url);
$username = substr($usernameandcountry, 0, -3);
$usercountry = substr($usernameandcountry, -2);

if ($username)
    $data = array('status' => 200, 'username' => $username, 'userflag' => $usercountry, 'message' => 'Name \o/ ');
else
    $data = array("status"=> '0', "message"=> 'Name NOT found :o( ');
echo json_encode($data);

function page_ebtname($url) {
    // if (file_exists($url))
        $fp = file_get_contents($url);
    // else return null;

    $res = preg_match("/<title>(.*)<\/title>/siU", $fp, $title_matches);
    if (!$res)
        return null;

    // Clean up title: remove EOL's and excessive whitespace.
    $title = preg_replace('/\s+/', ' ', $title_matches[1]);
    $title = trim($title);
    $name = substr(explode('(', $title)[1], 0, -1);

    $flagres = preg_match("/Informations utilisateur \(<\/b><a href=\"\/profile\/\?country=(.*)\"\>/siU", $fp, $flag_matches);
    if (!$flagres)
        return "coco";

    // Clean up flag, remove PHPSESSION ID
    $flag = preg_replace('/\+/', ' ', strstr($flag_matches[1], ';', TRUE));
    $flag = trim($flag);
    include_once 'cc.php';
    $iso_code = array_search(strtolower($flag), array_map('strtolower', $countries));

    return $name."-".$iso_code;
}

?>
