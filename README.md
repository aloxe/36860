# 36000 (ZEFG)
*Outils et carte pour les jeux Eurobilltracker en France | Tools and map for Eurobilltracker games in France*

This tool aims to help [Eurobilltracker.org](https://fr.eurobilltracker.com/) users to compete in the [game of the "36860 communes"](https://forum.eurobilltracker.com/viewtopic.php?f=34&t=7171). The aim of the game is to find euro banknotes in each and every one of the 36,000 french municipalities.

I was renamed *Ze Eurobilltracker french games* because it allows now to play several Eurobilltracker games related to France.

## Usage

Eurobilltracker.org provides a csv output of the banknotes you recorded on the site. Upload this file to the form in this tool and wait for the result. It then provides the list of municipalities visited and an access to the map of France with the area covered by the user.

Unknown locations are also saved and it is possible to fix mistakes thanks to links to OpenStreetMap and Eurobilltracker. Unknown locations that are correct but not an official municipality can be attached to a municipality and added to the software as a known location name.

## Dev

React refactor: To install the depending packages, use
This project uses React and was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Available Scripts
* `yarn`

Install the modules and dependencies

* `yarn start`

Runs the app in the dev mode on [http://localhost:3000](http://localhost:3000).

* `yarn test`

Launches the test runner in the interactive watch mode. (see [running tests](https://facebook.github.io/create-react-app/docs/running-tests))

* `yarn build`

The build will then need to be copied at the root of a web server with php.
- Copy all files at the root of your server or where you want to run it from.
- Create a folder `api/uploads` and grant it recursively public write permissions.

**Warning :** The map generation with all communes handles the display of more than 36,000 polygons and is a heavy process. It may slow down your browser or even freeze it.

## Public databases
- [Postcode database](https://www.data.gouv.fr/fr/datasets/5a9ac6b9c751df4caed2b133/) comes from the French Post [released under ODbL](https://www.data.gouv.fr/fr/datasets/base-officielle-des-codes-postaux/) enriched with 2019 Official Geographic Names by Etalab, using the same license.

- Population of municipalities come from the official state stats agency INSEE. [Data were published in csv](https://www.insee.fr/fr/statistiques/4265429?sommaire=4265511) in 2019 and converted here to Json.

- Altitude of municipalities come from a [codes postaux et codes INSEE correspondance project](https://www.data.gouv.fr/fr/datasets/correspondance-entre-les-codes-postaux-et-codes-insee-des-communes-francaises/) from Opendatasoft discontinued in 2016. (*partially copied here*)

- Surface of municipalities come from the [Villes Françaises project](https://sql.sh/736-base-donnees-villes-francaises) from SQL.sh. Data are from 2017. (*copied here*)

- Name of France régions, départements and their administrative HQ. Compiled from Wikipedia and [hand made here](https://raw.githubusercontent.com/aloxe/36000/master/geodata/departments_regions_france_2017.json).

- French map with municipality and département borders comes from the [france-geojson Project](https://github.com/gregoiredavid/france-geojson) which generates the maps from data released by INSEE (that keeps the official registry of municipalities) and IGN (that releases the official geographic administrative map of France once a year). Data are under [« Licence Ouverte / Open License »](https://www.etalab.gouv.fr/licence-ouverte-open-licence).

**TODO:** france-geojson was last updated in 2018 and miss the more recent changes from 01/01/2019. It is possible to get more up to date data from the [official API Géo](https://api.gouv.fr/api/api-geo.html) or [OpenStreetMap project 36000 Communes](http://prev.openstreetmap.fr/36680-communes) but both project have technical limitation.

## Possible future feature
- Use [Mapshot](https://r-spatial.github.io/mapview/reference/mapshot.html) to save user's map.
